## About This Profile

This profile instantiates POWDER's Massive MIMO system. 
Currently, this profile supports the following scenarios (with more to come):
1) Anechoic Chamber Setup: Allocates one Faros base station consisting of one 48-antenna array and two 2-antenna UEs inside an anechoic chamber. All these devices are connected to a d840 machine.
2) Outdoors Setup: Currently it allocates one Faros base station consisting of one 64-antenna array and provides the option to include client devices.

The profile also fetches the latest RENEWLab software with a wide variety of tools to work with the Faros massive MIMO system, including MATLAB scripts for over-the-air many-antenna experiments, large-scale channel measurement, and many python tools for test and experimentation. To learn more about RENEWLab, see the [RENEW Documentation](https://wiki.renew-wireless.org/).

## Getting Started

After logging into pc1, RENEWLab software source is available under the /scratch/ directory (cloned from [here](https://gitlab.renew-wireless.org)).
To use any of the design flows, including real-time channel measurement, matlab or python flows, follow the corresponding documentation pages in the [RENEW Documentation](https://wiki.renew-wireless.org/).